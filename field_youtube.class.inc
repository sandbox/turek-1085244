<?php
/**
 * @file
 * Field Youtube main class, for parsing youtube links and embeding media.
 */

class YouTube {
  protected $youtubeId = NULL;

  /**
   * Constructor method.
   *
   * This is the default constructor which accepts YouTube URL.
   */
  public function __construct($url = NULL) {
    if ($url != NULL) {
      $this->youtubeId = $this->parseURL($url);
    }
  }

  /**
   * Set YouTube ID method.
   *
   * This method sets YouTube ID.
   * Returns TRUE if the ID is ok, and FALSE if not.
   */
  public function setID($id) {
    if (preg_match('/([A-Za-z0-9_-]+)/', $url, $matches)) {
      $this->youtubeId = $id;
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get YouTube ID method.
   *
   * This method returns YouTube video ID. Otherwise returns NULL as set in 
   * constructor.
   */
  public function getID() {
    return $this->youtubeId;
  }

  /**
   * Parse YouTube URL and return video ID.
   *
   * This method strips YouTube ID from URL. Otherwise returns NULL.
   */
  public function parseURL($url) {
    if (preg_match('/watch\?v\=([A-Za-z0-9_-]+)/', $url, $matches)) {
      return $matches[1];
    }
    return FALSE;
  }

  /**
   * Get URL of YouTube video thumbnail.
   *
   * This method returns URL of YouTube video thumbnail. It can get one of three
   * defined by YouTube.
   */
  public function GetImg($url = NULL, $imgid = 1) {
    if ($url == NULL) {
      $videoid = $this->youtubeId;
    }
    else {
      $videoid = $this->parseURL($url);
      if (!$videoid) {
        $videoid = $url;
      }
    }
    return 'http://i1.ytimg.com/vi/' . $videoid . '/default.jpg';
  }

  /**
   * Get YouTube video HTML embed code.
   *
   * This method returns HTML code which is used to embed YouTube video in node.
   */
  public function outputEmbedVideo($width = 425, $height = 344, $hd = 1, $url = NULL) {
    if ($url == NULL) {
      $videoid = $this->youtubeId;
    }
    else {
      $videoid = $this->parseURL($url);
      if (!$videoid) {
        $videoid = $url;
      }
    }
    return '<object width="' . $width . '" height="' . $height . '">' .
      '<param name="movie" value="http://www.youtube.com/v/' . $videoid . '?hd=' . $hd . '"></param>' .
      '<param name="wmode" value="opaque"></param>' .
      '<param name="allowFullScreen" value="true">' .
      '<param name="allowScriptAccess" value="always">' .
      '<embed src="http://www.youtube.com/v/' . $videoid . '?hd=' . $hd . '" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" wmode="opaque" width="' . $width . '" height="' . $height . '"></embed>' .
    '</object>';
  }

  /**
   * Get YouTube screenshot HTML code.
   *
   * This method returns HTML code which is used to embed YouTube video 
   * thumbnail in page.
   */
  public function outputThumbImage($url = NULL, $imgid = 1, $alt = 'Video screenshot') {
    return '<img src="' . $this->GetImg($url, $imgid) . '" alt="' . $alt . '" title="' . $alt . '" />';
  }

  /**
   * Get YouTube video plain text URL.
   *
   * This method returns URL to YouTube video as a plain text.
   */
  public function outputPlainTextURL($hd = 1, $url = NULL) {
    if ($url == NULL) {
      $videoid = $this->youtubeId;
    }
    else {
      $videoid = $this->parseURL($url);
      if (!$videoid) {
        $videoid = $url;
      }
    }
    return 'http://www.youtube.com/watch?v=' . $videoid . '&hd=' . $hd;
  }
}
